# Laborordnung
Ergänzende allgemeine Betriebsanweisung im \
Zentrallabor für Mikro- und Nanotechnologie zu den Anlagen und Räumen von\
Compound Semiconductor Technology

Stand: **2022-08-16**

Es gilt die allgemeine Reinraumlaborordnung für das Zentrallabor für Mikro- und Nanotechnologie ([PDF Stand 2016-04](ZMNT%20-%20Reinraumlaborordnung%202016_04_20.pdf) [1]). Diese Laborordnung regelt spezifisches für die Räume und Anlagen vom Compound Semiconductor Technology.

Diese Laborordnung legt grundsätzliche Verhaltensweisen fest, gibt Hinweise auf besondere Gefährdungen und regelt den Umgang mit Gefahrstoffen. Sie ist verbindlich und muss allen Beschäftigten bekannt sein.Die Zugriffsmöglichkeit auf die Betriebsanweisungen ist  jederzeit sicherzustellen. Die Beschäftigten haben die einzelnen Vorgaben strikt zu beachten und einzuhalten.

## 1. Allgemeines
### 1.1. Aufenthalt
Personen, die nicht im Besitz der erforderlichen körperlichen und geistigen Voraussetzungen sind, kann die Arbeit im Labor nicht gestattet werden.

In Laboren darf nicht alleine gearbeitet werden. Zwischen 9 und 17 Uhr ist der Aufenthalt nur erlaubt, wenn mindestens zwei Personen anwesend sind.

Zwischen 17 und 9 Uhr ist der Aufenthalt nur erlaubt, wenn mindestens zwei Personen im Reinraum anwesend sind, davon muss mindestens eine Angestellt sein, d.h. kein Student (m/w/d).

Werdende und stillende Mütter unterstehen besonderem Schutz und dürfen das Labor nicht betreten.

Unbefugten ist der Zutritt zum Labor verboten.

### 1.2. Verhalten
Das Essen, Trinken und Rauchen im Labor ist untersagt. Die schließt Kaugummi ein.

Die Einrichtungen des Labors sind schonend zu behandeln. Mit dem Material ist sparsam umzugehen.

Jeder Benutzer ist verantwortlich für Ordnung und Sauberkeit an den Arbeitsplätzen. Der Arbeitsplatz ist jeweils nach Beendigung der Arbeit in ordnungsgemäßem Zustand (sauber, aufgeräumt und spannungsfrei) zu verlassen, Proben und Materialien sind an die entsprechenden Stellen zu räumen.

Den Anweisungen der Beschäftigten (Lehrende, wiss. und sonstige Mitarbeiter/innen) ist unbedingt Folge zu leisten.

Verkehrs-, Flucht- und Rettungswege sind frei zu halten. Das Abstellen von Gegenständen aller Art ist verboten. Die Brandschutzordnung [2] (Anhang B) ist zu beachten.

Laufende Prozesse dürfen nicht unbeobachtet bleiben. Es gilt Anwesenheitspflicht.

Beim Betreten (Verlassen) des Labors muss sich auf der vor der Schleuse aufgehängten Magnettafel eingeloggt (ausgeloggt) werden.

### 1.3. Unterweisung
Die vorherige Unterweisung in der Bedienung von Maschinen und Geräten ist Bedingung für deren Benutzung. Die jeweils angebrachten speziellen Hinweise sind zu beachten. 

Vor Aufnahme der Arbeiten müssen die Beschäftigten anhand der Betriebsanweisung arbeitsplatzbezogen unterwiesen werden. Inhalt und Zeitpunkt der Unterweisung sind schriftlich festzuhalten und von den Unterwiesenen durch Unterschrift zu bestätigen

Nicht nur im eigenen Interesse sind die Sicherheitsbestimmungen streng zu beachten. Personenschäden durch Unfall unterliegen den Haftungsgrundsätzen der Hochschule.

Im Labor dürfen nur die Arbeiten ausgeführt werden, die der jeweiligen Aufgabenstellung dienen und die mit der Professorin / dem Professor oder deren Beauftragten (Beschäftigten) abgesprochen sind.

Die Kenntnis der Laborordnung ist Bedingung für die Arbeit im Reinraum.

### 1.4. Arbeitsmaterial
Jegliche Geräte etc. die nicht zum Bestand des Labors gehören, dürfen nur mit Genehmigung der Laborleitung benutzt werden.

Beschädigungen, Verluste oder andere Besonderheiten an Laboreinrichtungen sind umgehend dem Laborpersonal zu melden. Beschädigungen und Verluste unterliegen den Haftungsgrundsätzen der Hochschule.

Gegenstände aus dem Bestand des Labors dürfen nur mit Genehmigung der Laborleitung für kurze Zeit gegen Quittung entliehen werden. 

Geräte, Material, Aufzeichnungen zu noch nicht fertigen Arbeiten sind an dem dafür vorgesehenen Platz sicher und mit Namenskennzeichnung aufzubewahren.

Es dürfen nur reinraumtaugliche Materialien (z.B. kein Holz) mit in den RR genommen werden.

Geht ein Verbrauchsmaterial zu neige, ist dies der Laborleitung mitzuteilen. Dies hat nicht erst beim letzten Verbrauchsgegenstand zu erfolgen, sondern wenn bereits ersichtlich ist, dass wenig Material vorhanden ist.

### 1.5. Notfalleinrichtungen
Zu den Notfalleinrichtungen gehören Personennotbrausen, Augenduschen, ggf. zusätzliche Augenspülflaschen, Handfeuerlöscher, Hauptschalter für Elektroversorgung, Gasabsperrventile, Verbandkästen.

Alle Notfalleinrichtungen dürfen weder verstellt noch verhängt werden. Sie sind gut erkennbar und frei zugänglich zu halten.

Alle Beschäftigten müssen die Standorte der Notfalleinrichtungen kennen und über ihre Funktion unterrichtet sein.

Personennotbrausen und Augenduschen sind monatlich zu prüfen. Die Prüfungen sind in eine Liste einzutragen.

Handfeuerlöscher, die benutzt oder auch nur angebraucht wurden, sowie beschädigte (auch bei beschädigter Plombe) sind zwecks Austausch umgehend bei der entsprechenden Stelle Dezernat 10 - Facility Management zu melden.

## 2. Schutzmaßnahmen und Verhaltensregeln
### 2.1. Gasalarm und Feueralarm
Entsprechend [1]: "Alle im Grauraum und Reinraum arbeitende Personen verlassen unverzüglich diese Räume auf dem kürzesten Weg" (s. Flucht- und Rettungswegeplan im Anhang 1 von [1])." 

### 2.2. Laboroverall u. Schuhwerk
Im Labor ist ein Overall zu tragen. Oberkörper, Arme und Beine müssen mit dem Overall bedeckt sein. Der Overall darf nur in der Schleuse an- und ausgezogen werden.

Es muss festes, geschlossenes und trittsicheres Schuhwerk mit einem Reinraum geeignetem Überschuh oder spezielles Reinraumschuhwerk getragen werden.

Schmutzige Overalls sind in die Wäschebox zu legen. Im Wandschrank in der Schleuse und im Schubladenschrank im Flur befindet sich Kleidung auf Lager; sollte sich der Vorrat dem Ende neigen, dem Personal Bescheid geben.

### 2.3. Kopfbedeckung, Mundschutz und Handschuhe
Im Labor ist ein Haarnetz zu tragen. In speziellen Bereichen ist eine Reinraumhaube zu tragen.

Im Labor ist ein Mundschutz zu tragen. 

Im Labor sind Latex- oder vergleichbare Handschuhe zu tragen. Bei Arbeiten mit Säuren, Laugen und weiteren gefährlichen Stoffen sind zusätzliche spezielle Schutzhandschuhe zu tragen. Diese sind vor der Benutzung auf ihr Tauglichkeit zu überprüfen (dürfen keine Risse und Löcher aufweisen).

Bei folgenden Arbeiten müssen ebenfalls spezielle Schutzhandschuhe getragen werden:
 - beim Umgang mit flüssigen Stickstoff,
 - beim Bedienen des Röhrenofens.

### 2.4. Schutzbrille
Bei vielen Arbeiten im Labor ist eine Schutzbrille zu tragen. Brillenträger benötigen eine optisch korrigierte Schutzbrille oder eine Überbrille über der Korrekturbrille.

Beispiele:
 - Beim Umgang mit Chemikalien (Lösungsmittel / Säuren / Laugen) aller Art,
 - beim Umgang mit flüssigen Stickstoff,
 - beim Umgang mit Photolacken,
 - beim Umgang mit rotierenden Geräten wie Lackschleuder, Lachenentwickler und Trockenschleuder.

### 2.5. Flowboxen
Flowboxen sollen den Arbeitsbereich möglichst Partikelfrei halten und ähnlich den Abzügen verhindern, dass gefährliche Stoffe beim Arbeiten in die Atemluft gelangen. In den Flowboxen dürfen nur Lösungsmittel / Stoffe die eine geringe Gefährlichkeit aufweisen, verwendet werden. Bei Ausfall der Abluft ist die Benutzung einzustellen. Apparaturen sind abzustellen (Kühlwasser muss ggf. weiterlaufen). Vorgesetzte sind zu informieren.

### 2.6. Abzüge
Abzüge in den Laboren sollen verhindern, dass gefährliche Stoffe beim Arbeiten in die Atemluft gelangen und den Benutzer gegen Verspritzen von gefährlichen Stoffen oder herumfliegenden Glassplittern schützen.

Die Abzüge sind nur voll wirksam, wenn die Front- und Seitenschieber geschlossen sind. Bei Arbeiten unter dem Abzug ist die Frontscheibe nicht mehr als notwendig zu öffnen. Der Kopf des Benutzers soll immer im Schutz der Scheibe bleiben. 

Nach Benutzung sind die Frontschieber der Abzüge zu schließen.

Schadstoffe dürfen auch in den Abzügen nur bei Störungsfällen oder beim befüllen der Apparatur frei werden. Überschüssige Reaktionsgase, Dämpfe, Aerosole oder Stäube, die bei normalem Arbeitsablauf entstehen, sind durch besondere Maßnahmen aufzufangen (z.B. durch entsprechende Waschflaschenanordnungen oder spezielle Filter).

Substanzen, die sehr giftige, giftige, krebserzeugende, erbgutverändernde, fortpflanzungsgefährdende, gesundheitsschädliche, ätzende oder brennbare Gase, Dämpfe, Aerosole oder Stäube abgeben können, dürfen nur im Abzug gehandhabt werden.

Bei Ausfall der Abluft ist die Benutzung einzustellen. Apparaturen sind abzustellen (Kühlwasser muss ggf. weiterlaufen). Vorgesetzte sind zu informieren.

### 2.7. Pipettieren
Pipettieren mit dem Mund ist ausnahmslos verboten.

### 2.8. Umgang mit Maschinen und elektrischen Anlagen.
Es ist verboten, Sicherheits- und Schutzeinrichtungen zu umgehen oder außer Betrieb zu nehmen.

Unter Spannung stehende Versuchsanordnung dürfen nicht sich selbst überlassen werden.

An Aufbauten, die zu Spitzenspannungen von über 40V führen können darf nur mit einer Hand gearbeitet werden!

Sicherheitskennzeichen beachten.

### 2.9. Vakuumarbeiten
Zum Schutz vor umherfliegende Glassplitter infolge von Implosionen sind Glasgefäße z.B. mit Schrumpf- oder Klebefolie, Schutzkorb, Schutzschild zu sichern. Das Gleiche gilt auch für Arbeiten mit Rotationsverdampfern. Sie sind im geschlossenen Abzug oder hinter einem Schutzschild durchzuführen.

### 2.10. Druckgasflaschen
Druckgasflaschen sind mit geeigneten Druckminderventilen zu betreiben. An Druckgasflaschen sind nach Gebrauch und auch nach dem Entleeren die Ventile zu schließen. 

Gefüllte und entleerte Flaschen dürfen nur mit aufgeschraubter Schutzkappe befördert werden. Druckgasflaschen mit Gefahrstoffen benötigen besondere Sicherheitsanforderungen, wie bspw. Aufbewahrung in Gefahrstoffschränken mit Abluft. Weitere Informationen zu Gefahrstoffen finden sich bei den jeweiligen Betriebsanweisungen.

### 2.11. Sonstiges
Glasbruch ist unter Verwendung der entsprechenden Abfallbehälter zu entsorgen.

Die Laboratorien sind sauber und in einem ordnungsgemäßen Zustand zu halten. Der Fußboden ist von abgestellten Gegenständen freizuhalten. Die Brandlasten sind auf ein Minimum zu begrenzen.

Der Arbeitsplatz ist jeweils nach Beendigung der Arbeit in ordnungsgemäßem Zustand (sauber und aufgeräumt) zu verlassen.

## 3. Umgang mit Gefahrstoffen
### 3.1. Gefahrstoffbezeichnung
Im Sinn der Verordnung (EG) Nr. 1272/2008 (CLP) sind Gefahrstoffe Stoffe oder Gemische, die in mindestens eine der folgenden Gefahrenklassen fallen:

#### Physikalische Gefahren
- Explosive Stoffe/Gemische und Erzeugnisse mit Explosivstoff,
- Entzündbare Gase,
- Aerosole,
- Oxidierende Gase,
- Gase unter Druck,
- Entzündbare Flüssigkeiten,
- Entzündbare Feststoffe,
- Selbstzersetzliche Stoffe oder Gemische,
- Pyrophore (selbstentzündliche) Flüssigkeiten,
- Pyrophore (selbstentzündliche)Feststoffe,
- Selbsterhitzungsfähige Stoffe oder Gemische,
- Stoffe oder Gemische, die bei Berührung mit Wasser entzündbare Gase abgeben,
- Oxidierende Flüssigkeiten,
- Oxidierende Feststoffe,
- Oxidierende Peroxide,
- Stoffe und Gemische, die gegenüber Metallen korrosiv sind
- Desensibilisierte explosive Stoffe/Gemische

#### Gesundheitsgefahren
- Akute Toxizität,
- Ätzwirkung auf die Haut/Hautreizung,
- Schwere Augenschädigung/Augenreizung,
- Sensibilisierung der Haut oder der Atemwege,
- Keimzellmutagenität,
- Karzinogenität,
- Reproduktionstoxizität,
- Spezifische Zielorgan-Toxizität (einmalige Exposition),
- Spezifische Zielorgan-Toxizität (wiederholte Exposition),
- Aspirationsgefahr,

#### Umweltgefahren
- Gewässergefährdend,
- Ozonschichtschädigung
 
### 3.2. Kennzeichnung
Sämtliche Behältnisse im Labor sind mit dem Namen ihres Inhaltes zu kennzeichnen.

Bei Gefahrstoffen gehören zu der Kennzeichnung mindestens zusätzlich zum Stoffnamen die Gefahrenbezeichnung und das Gefahrensymbol sowie die Nummern der H- und P-Sätze. Behälter von Abfallstoffen sind ebenfalls entsprechend ihrem Gefahrenpotential zu kennzeichnen. Für Becherglaser, in denen der Inhalt nur für einen begrenzten Zeitraum gelagert wird, genügt eine Beschriftung mit Namen, Datum und Benutzer.

### 3.3. Behälter und Gefäße
Gefäße, die Substanzen enthalten, müssen sofort gekennzeichnet werden mit: Inhalt, Einfülldatum und Name oder Kürzel des Einfüllenden.

Gefahrstoffe dürfen nicht in Behältnissen aufbewahrt werden, die zur Verwechslung mit Lebensmitteln führen können. Es ist untersagt, Gefahrstoffe in Gefäße abzufüllen, in denen üblicherweise Lebensmittel aufbewahrt werden.

Das Material des Behältnisses muss für die Aufbewahrung des betreffenden Stoffes geeignet sein.

### 3.4 Brennbare Flüssigkeiten
Brennbare Flüssigkeiten für den Handgebrauch dürfen nicht in Behältnissen über 1 Liter Fassungsvermögen aufbewahrt werden. Die Gesamtmenge soll pro Labor 10 Liter nicht überschreiten. Falls für den Fortgang der Arbeit größere Mengen unbedingt notwendig sind, sind diese in einem Sicherheitsschrank aufzubewahren.

### 3.5. Unterweisung
Die Laborbeschäftigten sind vor der Benutzung jeweils auf die besonderen Gefahren der Stoffe hinzuweisen. Persönliche Schutzausrüstung: Die in den Sicherheitsratschlägen (P-Sätzen) und in speziellen Betriebsanweisungen vorgegebenen Körperschutzmittel (z.B. Gesichtsschutz, Schutzbrille, Schutzhandschuhe, Handsalben), sind bereitzuhalten und zu benutzen.

Die Sicherheitsdatenblätter (SDB/MDS) sind zu beachten.

### 3.6. Entsorgung
Abfälle von Chemikalien sind getrennt in die dafür bereitstehenden Behälter zu entsorgen (z.B. organische Lösungsmittelbehälter für Aceton, Propanol, NMP). Begrenzte Mengen an Säuren und Laugen können über den Abfluss (Neutralisationsanlage) entsorgt werden; bei großen Mengen bzw. bei Sonderabfällen bitte Rücksprache mit dem Technischen Leiter bzw. Vorgesetzten halten!

Bei der Entsorgung in den im Reinraum aufgestellten Mülleimern ist darauf zu achten, dass diese in zwei Kategorien eingeteilt sind: Materialien mit Lösungsmitteln und Materialien mit Säuren und Laugen. Entsprechend verunreinigtes Material ist in den entsprechenden Mülleimer zu entsorgen.

### 3.7. Sonstiges
Vor dem Umgang mit Gefahrstoffen und vor der Durchführung von Arbeitsverfahren, bei denen möglicherweise Gefahrstoffe freigesetzt werden können, müssen das Gefahrenpotential ermittelt und die notwendigen Schutzmaßnahmen getroffen werden.

Gefahrstoffe sind so aufzubewahren und zu lagern, dass sie die menschliche Gesundheit und die Umwelt nicht gefährden.

Das Einatmen von Dämpfen und Stäuben sowie der Kontakt von Gefahrstoffen mit der Haut und Augen sind zu vermeiden. Beim offenen Umgang mit gasförmigen, staubförmigen oder solchen Gefahrstoffen, die bei niedrigen Temperaturen bereits ausgasen, ist grundsätzlich im Abzug zu arbeiten.

Chemikalien, insbesondere aber brennbare Flüssigkeiten, dürfen sich nur in begrenzten Mengen, die für den unmittelbaren Fortgang der Arbeit notwendig sind, während der Arbeitszeit am Arbeitsplatz befinden. Die Anzahl der Gefäße ist auf das unbedingt nötige Maß zu beschränken.

Alle angesetzten Flüssigkeiten, die nicht mehr benötigt werden, sind sachgerecht zu entsorgen.

Brennbare Flüssigkeiten nicht auf und in die Nähe von Heizplatten und o. ä. stellen bzw. aufbewahren.

Bei Ätzarbeiten sind spezielle Betriebsanweisungen zu beachten.

Persönliche Schutzausrüstung ist zu benutzen, z.B. Schutzbrille, Schutzhandschuhe und Schutzkittel.

Sicherheitskennzeichen sind zu beachten.

Nach der Benutzung sind die Chemikalien zu entsorgen und die Bechergläser in die Spülmaschine zu stellen. Bechergläser dürfen maximal zwei Tage im Abzug stehen, am Ende des zweiten Tages sind diese wegzuräumen.

## 4. Verhalten im Gefahrfall
Beim Auftreten gefährlicher Situationen ist folgendes zu beachten:

- Personenschutz geht vor Sachschutz.
- Ruhe bewahren und überstürztes unüberlegtes Handeln vermeiden.
- Gefährdete Personen warnen, ggf. zum Verlassen der Räume auffordern.
- Feuer: Bei Ausbruch eines Brandes ist die Brandschutzordnung [2] der Hochschule zu beachten und nach den dort festgelegten Regelungen zu verfahren.
- Soweit ohne persönliche Gefährdung möglich: Gefährdete Versuchs- und Verfahrensabläufe abstellen; Gas, Strom und ggf. Wasser abstellen (Kühlwasser muss weiterlaufen).

#### Insbesondere gilt
- Notruf auslösen, Tel.: 113
- Bis zum Eintreffen der Feuerwehr ist der Brand mit den vorhandenen Feuerlöschern zu bekämpfen, sofern dies gefahrlos möglich ist. Im Labor sind CO2-Feuerlöscher vorzuziehen.
- Veranlassen, dass Einsatzfahrzeuge der Feuerwehr auf der Straße erwartet und eingewiesen werden.
- Alle nicht an Lösch- oder Rettungsmaßnahmen beteiligten Personen haben den Gefahrenbereich zu verlassen.
- Fenster und Türen sind geschlossen zu halten.
- Aufzüge dürfen nicht benutzt werden.
- Kleiderbrände sind mit Feuerlöschern oder Notduschen zu löschen, bzw. mit Löschdecken zu ersticken.
- Wenn möglich, gleichzeitig gefährdete Personen aus Nachbarbereichen warnen und zum Verlassen der Räume auffordern.
 
#### Austreten gefährlicher Gase
 - Wenn möglich, Ventile schließen oder, wenn ohne Eigengefährdung möglich, für gute Durchlüftung sorgen.
 - Bei brennbaren Gasen Zündquellen vermeiden, Elektroschalter nicht betätigen.
 - Vorgesetzten Informieren.

#### Bei brennbaren Flüssigkeiten
 - Zündquellen vermeiden,
 - Elektroschalter nicht betätigen,
 - für gründliche Durchlüftung sorgen, soweit ohne persönliche Gefährdung möglich.
 - Mit Saug- oder Bindemitteln aufnehmen,
 - Ins Freie bringen oder dicht schließende Sammelbehälter verwenden und
 - Der Entsorgung zuführen.
 - Vorgesetzten informieren.
 
#### Bei ätzenden Flüssigkeiten
 - Gut lüften,
 - mit geeignetem Bindemittel aufnehmen
 - Vorgesetzten informieren.
 - Der Entsorgung zuführen.
 - Falls Verlassen der Räume erforderlich, nach Möglichkeit Apparaturen abstellen (außer Kühlwasser).

## 5. Erste Hilfe und Notrufnummern
 - Bei allen Hilfeleistungen auf die eigene Sicherheit achten.
 - Bei Unfällen, die zu leichten Verletzungen, Unwohlsein oder Hautreaktionen geführt haben, ist ein Arzt aufzusuchen. Jeder Elektrounfall ist einem Arzt vorzustellen.
 - Bei Unfällen mit schwerwiegenden Verletzungen, sowie mit Verletzungen, deren Art und Schwere nicht eingeschätzt werden kann, ist unverzüglich ein Notarzt zu alarmieren.
 - Bis zum Eintreffen des Notarztes Erste Hilfe leisten.
 - Ortskundige Personen am Eingang des Gebäudes postieren, die den Notarzt auf direktem Weg zum Verletzten führen.
 - Der Aushang Erste Hilfe zeigt Hinweise für Erste-Hilfe-Maßnahmen.

### 5.1. Erste-Hilfe-Maßnahmen
![Plakat Erste Hilfe](Plakat_Erste_Hilfe.jpg "Plakat Erste Hilfe") \

#### spezielle Maßnahmen
 - Bei Verbrennungen und Verätzungen ist sofort gründlich mit kaltem Wasser zu spülen! (Not- bzw. Augenduschen!)
 - Bei Flusssäureverätzungen nach der Erstversorgung, idealerweise mit Previn(NAME Korrekt?,) sollte zusätzlich mit Calciumgluconatgel (im Verbandkasten und der Chemie) eingerieben werden.

#### Notrufnummer
Notruf – Feuerwehr – Krankentransport: **113** 

## 6. Entsorgung und Ressourcenschonung
Die Mengen der verwendeten Chemikalien und Lösemittel sind auf das kleinstmögliche Maß einzuschränken. Hier gilt der Grundsatz "Verwertung vor Entsorgung".

Eine Belastung des Abwassers mit wassergefährdenden Stoffen ist zu verhindern.

Die Festlegungen zur getrennten Sammlung der Materialien sind unbedingt einzuhalten. Die Gefäße müssen ordnungsgemäß gekennzeichnet werden.

## 7. Weitere Arbeitshinweise
### 7.1. Lüftungswartung
Einmal im Monat wird in den Technologien vormittags die Lüftung gewartet (d.h. ausgeschaltet), in dieser Zeit ist der Aufenthalt im Labor untersagt. Im Walter-Schottky-Haus·findet die Lüftungswartung im Regelfall jeden 3. Montag im Monat, im Zentrallabor·für Technologie· im Regelfall jeden 4. Dienstag im Monat statt.

## 8. Verweise
[1] [Allgemeine Reinraumlaborordnung für das ZMNT Stand 2016-04](ZMNT%20-%20Reinraumlaborordnung%202016_04_20.pdf) \
[2] [Brandschutzordnung der RWTH (Intranet)](https://www9.rwth-aachen.de/awca/c.asp?id=btmk), siehe Anhang B, aufgerufen 2022-05-17 \
[3] [Brandschutzordnung Teil A (Aushang)](https://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaaajthg&download=1), siehe Anhang B, aufgerufen 2022-05-17 

## 9. Nachweis
Ich habe die vorstehende Laborordnung gelesen und vollständig verstanden.

____________________________________________________________________
Datum, Name, Unterschrift

## Anhang A - Liste der Änderungen
2013-06-10  Erstellung – Entwurf \
2014-03-13  Veröffentlicht \
2014-06-26  Gefahrstoffe, Materialverbrauch, Absätze \
2014-11-24  DGUV Information 213-850, Anwesenheitspflicht aus alter „Einweisung“ übernommen \
2015-01-15  kleinere Korrekturen Kap. 2 Flowbox & Abzüge \
2016-04-21  kleinere Anpassung zusammen mit der Einführung der Allgemeinen Reinraumlaborordnung \
2018-11-07  Aktualisierung auf Compound Semiconductor Technology, alte Gefahrstoffe entfernt, kleine Fehlerkorrekturen \
2019-01-16  Kleinere Änderungen \
2022-05-16  Übernahme in Gitlab (Layout Anpassung) \
2022-05-17  Aufräuen und Anpassung an die Realität (in Tendenz) \
2022-06-22  Test ob gelesen
2023-06-28  Test entfernt
2023-10-31  Unterschrifts"feld"

## Anhang B - Brandschutzordnung
### B.1. Brandschutzordnung Teil A (Aufgerufen 2022-05-17)
[Brandschutzordnung Teil A (Aushang)](https://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaaajthg&download=1), aufgerufen 2022-05-17 

### B.2. Brandschutzordnung Teil B und C (Aufgerufen 2022-05-17)
### Allgemeiner Teil
#### 1. Inhalt
Die Brandschutzordnung der RWTH Aachen dient dem vorbeugenden Brandschutz und regelt das Verhalten im Brandfall. Die Einhaltung der aufgeführten Regeln soll Brände, Explosionen und Brandkatastrophen sowie dadurch verursachte Personen- und Sachschäden möglichst verhindern oder begrenzen.

Diese Brandschutzordnung ist ein hochschulinternes Regelwerk und entbindet nicht von der Verpflichtung sonstige Arbeitsschutzvorschriften und allgemein anerkannte Regeln der Technik zu beachten, anzuwenden und einzuhalten.

Die Brandschutzordnung ist für alle Mitglieder und Angehörigen der RWTH Aachen sowie für sonstige Personen, die sich auf dem Gelände und in den Gebäuden der Universität aufhalten, verbindlich.
 
##### 1.1. Gliederung
Die Brandschutzordnung gliedert sich in drei Teile:
 
Teil A der Brandschutzordnung richtet sich an alle Personen (z.B. Mitarbeitende, Studierende, Besuchende) die sich in einem Gebäude aufhalten. Er enthält die wichtigsten Verhaltensregeln in schriftlicher Form. Dieser Teil A soll gut sichtbar an Stellen wie Informationstafeln, Fluren, Aufzügen, Hörsälen, Seminarräumen, Sozialräumen, Laboratorien oder Werkstätten ausgehängt werden.

Teil B der Brandschutzordnung inklusive der Anhänge richtet sich an alle Personen und Beschäftigten, die sich nicht nur vorübergehend an der RWTH Aachen aufhalten und muss diesen bekannt gemacht werden. Allen Beschäftigten ist bei Aufnahme ihrer Tätigkeit ein Exemplar der Brandschutzordnung Teil B über die Personalabteilungen zur Verfügung zu stellen.
 
Teil C der Brandschutzordnung richtet sich an alle Personen, denen über ihre allgemeinen Pflichten hinaus besondere Aufgaben im Brandschutz übertragen worden sind. Dies sind neben der Leitung der RWTH u.a. die Führungskräfte und Leitungen von Organisationseinheiten sowie Mitarbeitende mit einer besonderen Rolle in der Abwehr von Gefahren (z.B. Sicherheitsbeauftragte, Brandschutzhelfer*innen, Brandschutz-beauftragte und Gebäudeverantwortliche).
 
#### 2. Geltungsbereich
Die Brandschutzordnung gilt in allen Gebäuden, Einrichtungen und sonstigen Anlagen, welche der Hochschule zugeordnet sind, mit Ausnahme der Uniklinik.

#### 3. Schlussbestimmungen
Alle Mitglieder und Angehörigen der RWTH Aachen sind verpflichtet, an einer wirkungsvollen Brandverhütung mitzuwirken, entsprechend den hier genannten Regeln zu handeln und jeden Ausbruch eines Brandes unverzüglich den zuständigen Stellen zu melden.

Ordnung und Sauberkeit am Arbeitsplatz tragen erheblich zur Brandverhütung bei.
 
##### 3.1. Bekanntgabe der Brandschutzordnung
Die Brandschutzordnung ist in geeigneter Form durch die Verantwortlichen bekannt zu geben. Um bei einem möglichen Brandfall angemessen reagieren zu können, sind die in dieser Brandschutzordnung genannten Regeln und Verhaltensweisen im Rahmen der regelmäßig stattfindenden Unterweisungen anzusprechen und zu üben. Weitergehende Auskünfte und Informationen können beim Sachgebiet Brandschutz eingeholt werden. Ergänzend bietet das Sachgebiet Brandschutzübungen für die einzelnen Einrichtungen der RWTH Aachen an, in denen das richtige Verhalten im Brandfall, der Umgang mit Feuerlöschern sowie das richtige Verlassen des Gebäudes vermittelt wird.
 
##### 3.2. Verantwortlichkeiten
Alle Verantwortlichen, wie zum Beispiel geschäftsführende Leitung, Professorinnen und Professoren, Lehrbeauftragte, Leitungen von Arbeitskreisen und Werkstätten, Dezernatsleitungen und Leitung der Verwaltung veranlassen in ihrem Zuständigkeitsbereich alle notwendigen sowie im Einzelfall darüber hinausgehenden Maßnahmen und überwachen deren Durchführung.

Alle Mitarbeitenden sind verpflichtet, an ihrem Arbeitsplatz Maßnahmen zur Brandvorbeugung umzusetzen.
 
##### 3.3. Inkrafttreten
Die Brandschutzordnung der RWTH Aachen tritt am 20.05.2019 in Kraft.
  
(Rektoratsbeschluss vom 17.05.2019)

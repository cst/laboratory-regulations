# Laboratory Regulations
Supplementary general operating instructions in the \
Central laboratory for micro- and nanotechnology for the facilities and rooms of \
Compound Semiconductor Technology

Last update: **2022-08-16**

The "allgemeine Reinraumlaborordnung für das Zentrallabor für Mikro- und Nanotechnologie" apply to the Central Laboratory for Micro- and Nanotechnology ([PDF as of 2016-04](ZMNT%20-%20Reinraumlaborordnung%202016_04_20.pdf) [1]). This document regulates specific information for the rooms and systems of Compound Semiconductor Technology.

These laboratory regulations define basic behaviour, provide information on special hazards and regulate the handling of hazardous substances. They are binding and must be known to all employees. Access to the operating instructions must be ensured at all times. Employees must strictly observe and comply with the individual requirements.

## 1. General
### 1.1. 
Persons who are not in possession of the necessary physical and mental prerequisites cannot be permitted to work in the laboratory.

It is not allowed to work alone. Between 9 a.m. and 5 p.m., the stay is only allowed if at least two people are present.

Between 5 p.m. and 9 a.m., the stay is only allowed if at least two people are present in the cleanroom, of which at least one must be an employee, i.e. not a student (m/f/d).

Expectant and nursing mothers are subject to special protection and are not allowed to enter the laboratory.

Unauthorized persons are prohibited from entering the laboratory.

### 1.2. Conduct
Eating, drinking and smoking is prohibited in the laboratory. This includes chewing gum.

The laboratory facilities are to be treated with care. The material has to be used sparingly.

Each user is responsible for order and cleanliness at the workplaces. The workplace must be left in a properly condition (clean, tidy and free apllied voltage) after completion of the work, samples and materials are to be cleared to the appropriate places.

The instructions of the employees (lecturers, scientific and other staff) must be followed at all times.

Traffic, escape and rescue routes must be kept clear. Placing objects of any kind is prohibited. The fire protection regulations [2] (Appendix B) must be observed.

Ongoing processes must not remain unobserved. Attendance is compulsory.

When entering (leaving) the laboratory, you must log in (log out) on the magnetic board hung up in front of the lock.

### 1.3. Instructions
Prior instruction in the operation of machines and equipment is a condition for their use. The proper special instructions in each case have to be noticed.

Before starting work, the employees must be instructed based on the operating instructions in relation to the workplace. The content and time of the instruction must be recorded in writing and confirmed by the signatures of the persons instructed.

Safety regulations must be strictly observed, and not only in your own interest. Personal injuries due to accidents are subject to the university's liability principles.

In the laboratory, only work may be carried out that serves the respective task and that has been agreed upon with the professor or his or her representative (employee).

Knowledge of the laboratory regulations is a prerequisite for working in the clean room.

### 1.4. Material
Any equipment etc. that is not part of the laboratory's inventory may only be used with the approval of the laboratory management.

Damage, loss or other peculiarities of laboratory equipment must be reported immediately to the laboratory staff. Damage and losses are subject to the liability principles of the university.

Objects from the laboratory's stock may only be borrowed for a short time against receipt with the permission of the laboratory management. 

Equipment, materials, records of unfinished work shall be kept securely and marked with a name in the place provided for them.

Only cleanroom-suitable materials (e.g. no wood) may be taken into the cleanroom.

If a consumable is running low, the laboratory management must be informed. This must be done not only when the last consumable is used, but when it is already apparent that there is little material available.

### 1.5. Emergency Facilities
Emergency equipment includes people emergency showers, eye showers, if necessary, additionally eye wash bottles, hand-held fire extinguishers, main switches for electrical supply, gas shut-off valves, first-aid boxes.

All emergency facilities must not be obstructed or covered. They shall be kept clearly visible and freely accessible.

All employees must know the locations of the emergency facilities and be informed about their function.

People safety showers and eye showers must be checked monthly. The inspections are to be entered in a list.

Hand-held fire extinguishers that have been used or only slightly used, as well as damaged ones (even if the seal is damaged), must be reported immediately to the appropriate department (Dezernat 10 - Facility Management) for replacement.

## 2. Protective Measures and Rules of Conduct
### 2.1. Gas and Fire Alarm
According to [1]: "All persons working in the gray room and clean room leave these rooms immediately using the shortest route" (see escape and rescue route plan in Appendix 1 of [1])."

### 2.2. Lab Coat and Footwear
An overall must be worn in the laboratory. The upper part of the body, arms and legs must be covered with the overall. The overall may only be put on and taken off in the lock.

Sturdy closed and sure-footed footwear with a cleanroom-suitable overshoe or special cleanroom footwear must be worn.

Dirty overalls are to be put in the laundry box. Cleanroom clothing is stored in the lock and drawer cabinet in the hallway; inform the staff if the stock is running low.

### 2.3. Headgear, Face Mask and Gloves
A hair net must be worn in the laboratory. A cleanroom hood is to be worn in special areas.

A face mask must be worn in the laboratory.

Latex or comparable gloves must be worn when working in the laboratory. When working with acids, alkalis and other hazardous substances, additional special protective gloves must be worn. These must be checked for suitability before use (must not have any tears or holes).

Special protective gloves must also be worn for the following work:
- when handling liquid nitrogen,
- when operating the tube oven.

### 2.4 Safety Glasses
Safety goggles must be worn for many tasks in the laboratory. Wearers of glasses must wear over-glasses (basket glasses) over their own glasses or optically corrected safety glasses.

Examples:
- When handling chemicals (solvents / acids / alkalis) of all kinds,
- when handling liquid nitrogen,
- when handling photoresists,
- when handling rotating equipment such as resist centrifuges, resist developers and dry centrifuges.

### 2.5. Flowboxes
Flowboxes are intended to keep the work area as particle-free as possible and, similar to fume cupboards, prevent hazardous substances from entering the breathing air during work. Only solvents / substances with a low hazard level may be used in the flowboxes. If the exhaust air fails, use must be discontinued. Equipment must be shut down (cooling water must continue to run if necessary). Inform a supervisor.

### 2.6 Fume Hoods
Fume hoods in laboratories are designed to prevent hazardous substances from entering the air breathed during work and to protect the user against splashes of hazardous substances or flying glass splinters.

The fume hoods are only fully effective when the front and side sliders are closed. When working under the fume hood, do not open the front glass more than necessary. The user's head should always remain in the protection of the glass. 

After use, the front sliders of the fume hood must be closed.

Pollutants may also be released in the fume hoods only in the event of malfunctions or when filling the apparatus. Excess reaction gases, vapours, aerosols or dusts produced during normal work must be collected by special measures (e.g., by appropriate washing bottle arrangements or special filters).

Hazardous substances which may give off very toxic, poisonous, carcinogenic, mutagenic, toxic to reproduction, harmful to health, corrosive or flammable gases, vapours, aerosols or dusts may only be handled in the fume hood.

If the ventilation fails, use must be discontinued. Equipment must be shut down (cooling water must continue to run if necessary). Inform supervisor.

### 2.7. Pipetting
Pipetting by mouth is prohibited without exception.

### 2.8.  Handling Machines and Electrical Equipment
It is prohibited to bypass or disable safety and protective devices.

Experimental set-ups under voltage must not be left to themselves.

Work on superstructures that can lead to peak voltages of over 40V must only be carried out with one hand!

Pay attention to safety signs.

##2.9. Tasks with Vacuum
To protect against flying glass splinters as a result of implosions, glass vessels must be secured e.g. with shrink or adhesive foil, protective cage, protective shield. The same applies to work with rotary evaporators. They must be carried out in a closed fume hood or behind a protective shield.

### 2.10. Compressed gas cylinders
Compressed gas cylinders must be operated with suitable pressure reducing valves. The valves on compressed gas cylinders must be closed after use and after emptying.

Filled and emptied cylinders may only be transported with the protective cap screwed on. Compressed gas cylinders containing hazardous substances require special safety requirements, e.g. storage in hazardous substance cabinets with exhaust air. Further information on hazardous substances can be found in the respective operating instructions.

### 2.11. Other
Broken glass must be disposed of using the appropriate waste containers.

The laboratories are to be kept clean and in a proper condition. The floor is to be kept free of deposited objects. Fire loads are to be kept to a minimum.

The workplace must be left in an orderly condition (clean and tidy) at the end of the work.

## 3. Handling Hazardous Substances
### 3.1. Hazardous Substance Designation
For the purposes of the Regulation (EG) No. 1272/2008 (CLP), hazardous substances are substances or mixtures that fall into at least one of the following hazard classes:
 
#### Physical Hazards
- Explosive,
- Flammable gas,
- Flammable aerosol,
- Oxidizing gas,
- Gases under pressure,
- Flammable liquid,
- Flammable solid,
- Self-reactive substance or mixture,
- Pyrophoric liquid,
- Pyrophoric solid,
- Self-heating substance or mixture,
- Substance or mixture which in contact with water emit flammable gas,
- Oxidizing liquid,
- Oxidising solid,
- Organic peroxide,
- Substance or mixture corrosive to metals
- Desensitized explosive mixtur _?_

#### Health Hazards
- Acute toxicity,
- Skin corrosion/irritation,
- Serious eye damage/eye irritation,
- Respiratory/skin sensitisation,
- Germ cell mutagenicity,
- Carcinogenicity,
- Reproductive toxicity,
- Specific target organ toxicity - single exposure,
- Specific target organ toxicity - repeated exposure,
- Aspiration hazard

#### Enviroment Hazards
- Hazardous to the aquatic enviroment,
- Hazardous for the ozone layer

### 3.2. Labelling
All containers in the laboratory must be labelled with the name of their contents.

In the case of hazardous substances, the labelling shall include at least, in addition to the substance name, the hazard designation and the hazard symbol as well as the numbers of the H- and P-phrases. Containers of waste substances must also be labelled according to their hazard potential. For beakers in which the contents are only stored for a limited period of time, labelling with the name, date and user is sufficient.

### 3.3. Containers and Vessels
Vessels containing substances must be labelled immediately with: Contents, date of filling and name or abbreviation of the person who filled the container.

Hazardous substances must not be stored in containers that could lead to confusion with food. It is prohibited to fill hazardous substances into containers in which foodstuffs are usually stored.

The material of the container must be suitable for the storage of the substance concerned.

### 3.4. Flammable Liquids
Flammable liquids for hand use must not be stored in containers with a capacity of more than 1 litre. The total quantity should not exceed 10 litres per laboratory. If larger quantities are absolutely necessary for the progress of the work, they must be stored in a safety cabinet.

### 3.5. Instruction
The laboratory workers must be informed of the particular hazards of the substances before use. Personal protective equipment: The personal protective equipment (e.g. face shield, safety goggles, protective gloves, hand ointments) specified in the safety advices (P-phrases) and in special operating instructions must be kept available and used.

The safety data sheets (SDB/MDS) must be observed.

### 3.6. Disposal
Waste chemicals must be disposed separately in the containers provided for this purpose (e.g. organic solvent containers for acetone, propanol, NMP). Only small quantities of acids and alkalis can be disposed via the drain (neutralisation plant); for larger quantities or special waste, please consult the technical manager or supervisor!

When disposing of waste in the bins placed in the cleanroom, make sure that they are divided into two categories: Materials with solvents and materials with acids and alkalis. Contaminated material is to be disposed in the corresponding waste bins.

### 3.7. Other
Before handling hazardous substances and before carrying out work procedures during which hazardous substances may be released, the hazard potential must be determined and the necessary protective measures taken.

Hazardous substances shall be kept and stored in such a way that they do not endanger human health and the environment.

Avoid inhalation of vapours and dusts and contact of hazardous substances with skin and eyes. When openly handling gaseous, dusty or hazardous substances which already outgas at low temperatures, work must always be carried out in a fume hood.

Chemicals, but especially flammable liquids, may only be present at the workplace during working hours in limited quantities that are necessary for the immediate progress of the work. The number of containers shall be limited to what is strictly necessary.

Properly dispose of all prepared liquids that are no longer required.

Do not place or store flammable liquids on or near hot plates or similar objects..

Special operating instructions must be observed for etching work.

Personal protective equipment must be used, e.g. safety goggles, protective gloves and protective gowns.

Safety signs must be respected.

After use, the chemicals must be disposed of and the beakers placed in the dishwasher. Beakers may be left in the fume hood for a maximum of two days, at the end of the second day they must be cleared away.

## 4. Behaviour in Case of danger
When dangerous situations occur, you must pay attention to the following:

- Personal protection takes precedence over property protection.
- Keep calm and avoid hasty, ill-considered action.
- Warn endangered persons, if necessary ask them to leave the rooms.
- Fire: In the event of a fire, the university's fire protection regulations must be observed [2] and the rules laid down therein must be followed.
- As far as possible without personal danger: Shut down endangered experimental and procedural processes; turn off gas, electricity and, if necessary, water (cooling water must continue to run).

#### In particular
- Make an emergency call, Tel.: 113
- Until the fire brigade arrives, the fire must be fought with the fire extinguishers available, provided this can be done safely. CO2 fire extinguishers are preferable in the laboratory.
- Arrange for fire brigade emergency vehicles to be awaited and briefed on the road.
- All persons not involved in extinguishing or rescue measures must leave the dangerous area.
- Windows and doors must be kept closed.
- Lifts are not allowed to be used.
- Clothing fires must be extinguished with fire extinguishers or emergency showers or smothered with fire blankets.
- If possible, simultaneously warn endangered persons from neighbouring areas and ask them to leave the rooms.

#### Leakage of Hazardous Gas
- If possible, close valves or, if possible, without endangering oneself, ensure good ventilation.
- In the case of flammable gases, avoid ignition sources, do not operate electric switches.
- Inform superiors.

#### For Flammable Liquid
- Avoid ignition sources,
- Do not operate electric switches,
- Ensure thorough ventilation as far as possible without personal danger,
- Absorb with absorbents or binders,
- Bring out into the open air or use tightly closing collection containers and
- Send for disposal.
- Inform supervisor.

#### For Corrosive Liquids
- Ventilate well,
- Absorb with a suitable binding agent and
- Inform supervisor.
- Send for disposal.
- If it is necessary to leave the rooms, turn off the equipment if possible (except cooling water).

## 5. First Aid and Emergency Numbers
- Pay attention to your own safety when providing assistance.
- In case of accidents resulting in minor injuries, indisposition or skin reactions, a doctor must be consulted. Every electrical accident must be presented to a doctor.
- In the event of accidents involving serious injuries and injuries whose nature and severity cannot be assessed, an emergency doctor must be alerted immediately.
- Give first aid until the arrival of the emergency doctor.
- Let people who know the area at the entrance of the building wait to guide the emergency doctor directly to the injured person.
- The First Aid notice board shows instructions for first aid measures.

### 5.1. First Aid
![Plakat Erste Hilfe](Plakat_Erste_Hilfe.jpg "Plakat Erste Hilfe") \

#### Specific Measures
- In case of burns or chemical burns, immediately rinse thoroughly with cold water! (Emergency or eye showers!)
- In the case of hydrofluoric acid burns, calcium gluconate gel (see first aid kit) should also be rubbed in.

#### Emergency Number
Emergency call - Fire brigade - Ambulance service **113**

## 6. Disposal and resource conservation
The quantities of chemicals and solvents used must be kept to the minimum possible. The principle of " utilization before disposal" applies here.

Pollution of the wastewater with substances hazardous to water must be prevented.

The specifications for the separate collection of materials must be strictly adhered to. The containers must be properly labelled.

## 7. Further Instructions
### 7.1. Ventilation Maintenance
Once a month, the ventilation in the technologies is serviced (i.e. switched off) in the morning, during which time it is forbidden to be in the laboratory. In the Walter-Schottky-Haus, ventilation maintenance usually takes place every 3rd Monday of the month, in the Central Laboratory for Technology usually every 4th Tuesday of the month.

## 8. Referrals
[1] [Allgemeine Reinraumlaborordnung für das ZMNT Stand 2016-04](ZMNT%20-%20Reinraumlaborordnung%202016_04_20.pdf) \
[2] [Brandschutzordnung der RWTH (Intranet)](https://www9.rwth-aachen.de/awca/c.asp?id=btmk), siehe Appendix B, aufgerufen 2022-05-17 \
[3] [Brandschutzordnung Teil A (Aushang)](https://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaaajthg&download=1), siehe Appendix B, aufgerufen 2022-05-17 

## 9. Proof
I have read and understood all of the above laboratory regulations.

____________________________________________________________________
Date, Name, Signature

## Appendix A - List of Changes
2013-06-10	Creation - Draft \
2014-03-13	Published \
2014-06-26	Hazardous substances, material consumption, paragraph \
2014-11-24	DGUV Information 213-850, compulsory attendance taken over from old "briefing” \
2015-01-15	Minor corrections Chap. 2 Flowbox & deductions \
2016-04-21	Minor adjustment together with the introduction of the General Cleanroom Laboratory Regulations \
2018-11-07  Update to Compound Semiconductor Technology, old hazardous substances removed, minor error corrections. \
2019-01-16  Minor changes \
2021-12-17  Translated into English \
2022-05-18  Adaptation to the current proposal \
2022-06-22  Check if read
2023-06-28  removed check
2023-10-31  added signature "field"

## Appendix B
See german version!

































